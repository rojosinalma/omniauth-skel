class Authorization < ActiveRecord::Base
  validates_presence_of :uid, :provider
  validates_uniqueness_of :uid, scope: :provider

  def self.from_omniauth(auth)
    where(provider: auth.provider, uid: auth.uid).first_or_create do |new_auth|
      new_auth.provider         = auth.provider
      new_auth.uid              = auth.uid
      new_auth.token            = auth.credentials.token
      new_auth.secret           = auth.credentials.secret
      new_auth.refresh_token    = auth.credentials.refresh_token
      new_auth.token_expires_at = Time.at(auth.credentials.expires_at)
      new_auth.email            = auth.info.email
      new_auth.username         = auth.info.nickname || auth.info.nickname || auth.info.name

      new_auth.save!
    end
  end
end
